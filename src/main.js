import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'
import store from './store'
import './plugins/base' 

import VueAxios from 'vue-axios'
import Axios from 'axios'

const moment = require('moment');
require('moment/locale/es');


Vue.use( VueAxios,Axios );
Vue.config.productionTip = false
Vue.use(require('vue-moment'), {
    moment
});

new Vue({
    vuetify,
    store,
    router,
    render: h => h(App)
}).$mount('#app')
