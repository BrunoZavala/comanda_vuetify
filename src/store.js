import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist';

Vue.use(Vuex)
const vuexPersist = new VuexPersist({
    key: "my-app",
    storage: localStorage
});

export default new Vuex.Store({
    state: {
        Sidebar_drawer: null,
        Customizer_drawer: false,
        SidebarColor: 'white',
        SidebarBg: '',
        user: {
            logged: 0,
            data: ''
          },
          invoice: [],
          proforma: [],
          ordenpago: [],
          items: [],
          comprobante: 0
      },
    mutations: {
        SET_SIDEBAR_DRAWER (state, payload) {
            state.Sidebar_drawer = payload
        },
        SET_CUSTOMIZER_DRAWER (state, payload) {
            state.Customizer_drawer = payload
        },
        SET_SIDEBAR_COLOR (state, payload) {
            state.SidebarColor = payload
        },
        loginuser(state, data) {
            state.user.data = data;
            state.user.logged = 1;
        },
        logoutuser(state) {
            state.user.data = '';
            state.user.logged = 0;
        } 
    },
    getters:{
        userData: state => {
            return state.user.data.user
        },
        userToken: state => {
            return state.user.data.token
        },
    },
    actions: {

    },
    plugins: [vuexPersist.plugin]
})